[![Build Status](https://travis-ci.org/hudson-s/opencam.svg?branch=master)](https://travis-ci.org/hudson-s/opencam)
[![Coverage Status](https://coveralls.io/repos/github/hudson-s/opencam/badge.svg?branch=master)](https://coveralls.io/github/hudson-s/opencam?branch=master)
# opencam

Wrapper em Golang para uso da API de Dados Abertos da Câmara

>Saiba mais sobre os dados abertos da Câmara em: https://dadosabertos.camara.leg.br/


**Funções disponíveis até o momento:**
- Informações sobre partidos politicos
- Informações sobre deputados

## Uso

```
  go get -u github.com/hudson-s/opencam
```

## Exemplo

```go
package main

import (
	"fmt"

	"github.com/hudson-s/opencam"
)

func main() {
	d := opencam.Deputies{}
	nome := "joao"
	id := 0

	// Busca por nome e retorna um conjunto com deputados encontrados
	dadosDeputado := d.GetDeputiesByName(nome)

	if len(dadosDeputado) > 0 {
		id = dadosDeputado[0].ID
	}

	// Captura as informações completas
	infoDeputado := d.GetDetailsDeputy(id)

	fmt.Printf("%+v", infoDeputado)
}
```
