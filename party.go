package opencam

import (
	"fmt"
	"strings"
)

// PoliticalParty contains all the function Political Party
type PoliticalParty struct {
}

// BASIC INFORMATION ON POLITIAL PARTY:

// BasicPoliticalParty contains basic info about political parties
type BasicPoliticalParty struct {
	ID      int    `json:"id"`
	Acronym string `json:"sigla"`
	Name    string `json:"nome"`
	URI     string `json:"uri"`
}

type elementalPoliticalParty struct {
	Data []BasicPoliticalParty `json:"dados"`
}

// GetPartyByAcronyms search Political Parties by acronym
// Returns a slice of basic information about Parties - sorted by acronym
func (p PoliticalParty) GetPartyByAcronyms(acronyms ...string) []BasicPoliticalParty {
	url := fmt.Sprintf("%spartidos?sigla=%s&ordenarPor=sigla", BaseURL, strings.Join(acronyms, "&sigla="))
	var basicInfo elementalPoliticalParty
	getResponse(url, &basicInfo)
	return basicInfo.Data
}

// DETAILED INFORMATION POLITIAL PARTY: partyLeader, partyIDStatus AND DetailsPoliticalParty

type partyLeader struct {
	URI           string `json:"uri"`
	Name          string `json:"nome"`
	AcronymParty  string `json:"siglaPartido"`
	URIParty      string `json:"uriPartido"`
	UF            string `json:"uf"`
	IDlegislature int    `json:"idLegislatura"`
	URLPhoto      string `json:"urlFoto"`
}

// partyIDStatus contains party status
type partyIDStatus struct {
	Date            string      `json:"data"`
	IDlegislature   string      `json:"idLegislatura"`
	Situation       string      `json:"situacao"`
	TotalPossession string      `json:"totalPosse"`
	TotalMembers    string      `json:"totalMembros"`
	URIMembers      string      `json:"uriMembros"`
	Leader          partyLeader `json:"lider"`
}

// DetailsPoliticalParty contains all party information
type DetailsPoliticalParty struct {
	ID              int           `json:"id"`
	Acronym         string        `json:"sigla"`
	Name            string        `json:"nome"`
	URI             string        `json:"uri"`
	Status          partyIDStatus `json:"status"`
	ElectoralNumber int           `json:"numeroEleitoral"`
	URLLogo         string        `json:"urlLogo"`
	URLWebSite      string        `json:"urlWebSite"`
	URLFacebook     string        `json:"urlFacebook"`
}

// FullPoliticalParty contains all party information
type fullPoliticalParty struct {
	Data DetailsPoliticalParty `json:"dados"`
}

// GetDetailsParty Complete information about a political party
// Returns a struct DetailsPoliticalParty
func (p *PoliticalParty) GetDetailsParty(ID int) DetailsPoliticalParty {
	url := fmt.Sprintf("%spartidos/%d", BaseURL, ID)
	var details fullPoliticalParty
	getResponse(url, &details)
	return details.Data
}
