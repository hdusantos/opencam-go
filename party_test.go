package opencam

import (
	"reflect"
	"testing"
)

// Struct to test GetPartyByAcronyms
type acronymsTest struct {
	input    []string
	expected []BasicPoliticalParty
}

// Struct to test GetDetailsParty
type idTest struct {
	input    int
	expected DetailsPoliticalParty
}

func TestGetBasicByAcronyms(t *testing.T) {
	test1 := []acronymsTest{
		{
			input:    []string{"pmdb"},
			expected: []BasicPoliticalParty{{36800, "PMDB", "Partido do Movimento Democrático Brasileiro", "https://dadosabertos.camara.leg.br/api/v2/partidos/36800"}},
		},
		{
			input:    []string{"pt"},
			expected: []BasicPoliticalParty{{36844, "PT", "Partido dos Trabalhadores", "https://dadosabertos.camara.leg.br/api/v2/partidos/36844"}},
		},
		{
			input:    []string{"pv"},
			expected: []BasicPoliticalParty{{36851, "PV", "Partido Verde", "https://dadosabertos.camara.leg.br/api/v2/partidos/36851"}},
		},
		{
			input: []string{"pv", "pt", "pmdb"},
			expected: []BasicPoliticalParty{
				{36800, "PMDB", "Partido do Movimento Democrático Brasileiro", "https://dadosabertos.camara.leg.br/api/v2/partidos/36800"},
				{36844, "PT", "Partido dos Trabalhadores", "https://dadosabertos.camara.leg.br/api/v2/partidos/36844"},
				{36851, "PV", "Partido Verde", "https://dadosabertos.camara.leg.br/api/v2/partidos/36851"}},
		},
		{
			input: []string{},
			expected: []BasicPoliticalParty{
				{
					36898,
					"AVANTE",
					"Avante",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36898",
				},
				{
					36769,
					"DEM",
					"Democratas",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36769",
				},
				{
					36899,
					"MDB",
					"Movimento Democrático Brasileiro",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36899",
				},
				{
					37900,
					"PATRI",
					"Patriota",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/37900",
				},
				{
					36779,
					"PCdoB",
					"Partido Comunista do Brasil",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36779",
				},
				{
					36786,
					"PDT",
					"Partido Democrático Trabalhista",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36786",
				},
				{
					36761,
					"PEN",
					"Partido Ecológico Nacional",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36761",
				},
				{
					36793,
					"PHS",
					"Partido Humanista da Solidariedade",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36793",
				},
				{
					36887,
					"PMB",
					"Partido da Mulher Brasileira",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36887",
				},
				{
					36800,
					"PMDB",
					"Partido do Movimento Democrático Brasileiro",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36800",
				},
				{
					36801,
					"PMN",
					"Partido da Mobilização Nacional",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36801",
				},
				{
					36896,
					"PODE",
					"Podemos",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36896",
				},
				{
					36809,
					"PP",
					"Partido Progressista",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36809",
				},
				{
					36762,
					"PPL",
					"Partido Pátria Livre",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36762",
				},
				{
					36813,
					"PPS",
					"Partido Popular Socialista",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36813",
				},
			},
		},
		{input: []string{"jhbfds"},
			expected: []BasicPoliticalParty{}},

		{input: []string{"jhbfds", "kjgsd", ""},
			expected: []BasicPoliticalParty{}},
	}

	p := PoliticalParty{}

	for _, itest := range test1 {
		current := p.GetPartyByAcronyms(itest.input...)
		if !reflect.DeepEqual(current, itest.expected) {
			t.Errorf("\nExpected value:\n%+v\nOutput:\n%+v", itest.expected, current)
		}
	}
}

func TestGetDetailsByID(t *testing.T) {
	test1 := []idTest{
		{
			input: 36769,
			expected: DetailsPoliticalParty{
				ID:      36769,
				Acronym: "DEM",
				Name:    "Democratas",
				URI:     "https://dadosabertos.camara.leg.br/api/v2/partidos/36769",
				Status: partyIDStatus{
					Date:            "2018-02-06 21:10:33.513",
					IDlegislature:   "55",
					Situation:       "Ativo",
					TotalPossession: "21",
					TotalMembers:    "44",
					URIMembers:      "https://dadosabertos.camara.leg.br/api/v2/deputados?legislatura=55&partido=DEM",
					Leader: partyLeader{
						URI:           "https://dadosabertos.camara.leg.br/api/v2/deputados/88950",
						Name:          "RODRIGO GARCIA",
						AcronymParty:  "DEM",
						URIParty:      "https://dadosabertos.camara.leg.br/api/v2/partidos/36769",
						UF:            "SP",
						IDlegislature: 55,
						URLPhoto:      "http://www.camara.gov.br/internet/deputado/bandep/88950.jpg",
					},
				},
				ElectoralNumber: 0,
				URLLogo:         "http://www.camara.leg.br/internet/Deputado/img/partidos/DEM.gif",
				URLWebSite:      "",
				URLFacebook:     "",
			},
		},
		{
			input:    0,
			expected: DetailsPoliticalParty{},
		},
	}
	p := PoliticalParty{}

	for _, itest := range test1 {
		current := p.GetDetailsParty(itest.input)
		if current != itest.expected {
			t.Errorf("\nExpected value:\n%+v\nOutput:\n%+v", itest.expected, current)
		}
	}
}
